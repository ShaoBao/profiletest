//
//  TabBarViewController.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        createTabs()
        tabBar.isTranslucent = false
        tabBar.backgroundColor = .white
    }
    
    private func createTabs() {
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
        setViewControllers([createSearchTab(), createTestVC1Tab(),createProfileTab(),createTestVC2Tab(),createTestVC3Tab()], animated: true)
    }
    
    private func createSearchTab() -> UIViewController {
        let searchVC = SearchViewController()
        searchVC.extendedLayoutIncludesOpaqueBars = true
        searchVC.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
        let navigationController = UINavigationController(rootViewController: searchVC)
        //        navigationController.setNavigationBarHidden(false, animated: true)
        navigationController.tabBarItem = UITabBarItem(title: nil, image: .strokedCheckmark, selectedImage: .strokedCheckmark)
        navigationController.tabBarItem.tag = 0
        return navigationController
    }
    
    private func createProfileTab() -> UIViewController {
        let userProfileIcon = UIImage(named: "profile")
        let userViewController = UserAssembly.configureModule()
        userViewController.extendedLayoutIncludesOpaqueBars = true
        let navigationController = UINavigationController(rootViewController: userViewController)
        navigationController.tabBarItem = UITabBarItem(title: nil, image: userProfileIcon, selectedImage: userProfileIcon)
        return navigationController
    }
    
    private func createTestVC1Tab() -> UIViewController {
        let userViewController = TestVC1()
        userViewController.extendedLayoutIncludesOpaqueBars = true
        let navigationController = UINavigationController(rootViewController: userViewController)
        navigationController.tabBarItem = UITabBarItem(title: nil, image: .add, selectedImage: .add)
        return navigationController
    }
    private func createTestVC2Tab() -> UIViewController {
           let userViewController = TestVC2()
           userViewController.extendedLayoutIncludesOpaqueBars = true
           let navigationController = UINavigationController(rootViewController: userViewController)
        navigationController.tabBarItem = UITabBarItem(title: nil, image: .remove, selectedImage: .remove)
           return navigationController
       }
    private func createTestVC3Tab() -> UIViewController {
              let userViewController = TestVC3()
              userViewController.extendedLayoutIncludesOpaqueBars = true
              let navigationController = UINavigationController(rootViewController: userViewController)
        navigationController.tabBarItem = UITabBarItem(title: nil, image: .strokedCheckmark, selectedImage: .strokedCheckmark)
              return navigationController
          }
}

//
//  CustomIVewModel.swift
//  TableTest
//
//  Created by Gleb on 14/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation



struct  CustomViewModel {
    struct TableViewCellModel {
        var naming: String
        var description: String
    }
    var models: [TableViewCellModel] = []
}

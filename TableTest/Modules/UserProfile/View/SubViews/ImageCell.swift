//
//  ImageCell.swift
//  TableTest
//
//  Created by Gleb on 14/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit

class UserImageCell: UITableViewCell {
    
    
    var userImage: UIImageView?
    var separator: UIView?
    
    var layout = ProCellTableLayout()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        userImage = UIImageView()
        userImage?.contentMode = .redraw
        userImage?.image = UIImage(named: "profile")
        addSubview(userImage!)
        layout.initial(userImage: userImage!)
        setupSeparator()
    }
    
    private func setupSeparator() {
        separator = UIView()
        separator?.backgroundColor = .lightGray
        separator?.alpha = 0.5
        
        addSubview(separator!)
        separator?.snp.makeConstraints {
            
            
            $0.left.equalTo(self.snp.left).offset(16)
            $0.right.equalTo(self.snp.right).offset(-16)
            
            $0.height.equalTo(1)
            $0.top.equalTo((self.snp.bottom)).offset(8)
        }
    }
    
    
}

//
//  ProfessionCell.swift
//  TableTest
//
//  Created by Gleb on 14/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import  UIKit



class ProfessionCell: UITableViewCell {
    
    var leftLabel: UILabel?
    var proCellLayout = ProCellTableLayout()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLeftLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    private func setupLeftLabel() {
        leftLabel = UILabel()
        leftLabel?.text = "Professional Information:"
        leftLabel?.numberOfLines = 0
        leftLabel?.font = UIFont.systemFont(ofSize: 18)
        leftLabel?.lineBreakMode = .byWordWrapping
        addSubview(leftLabel!)
        leftLabel?.textColor = .black
        proCellLayout.professionalInfoInit(leftLabel!)
    }
}

//
//  ProCell.swift
//  TableTest
//
//  Created by Gleb on 14/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import UIKit

class ProCell: UITableViewCell {
    
    var leftLabel: UILabel?
    var rightLabel: UILabel?
    var userImage: UIImageView?
    var separator: UIView?
    
    var viewModel:CustomViewModel.TableViewCellModel? {
        didSet {
            updateSubViews()
        }
    }
    
    private func updateSubViews() {
        leftLabel?.text = viewModel?.naming
        rightLabel?.text = viewModel?.description
        setupSeparator()
        self.layoutIfNeeded()
    }
    var proCellLayout =  ProCellTableLayout()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureProfessionCell() {
        leftLabel?.text = "Professional Information"
        leftLabel?.textColor = .black
    }
    
    private func setupSubViews() {
        setupLeftLabel()
        setupRightLabel()
    }
    
    private func setupSeparator() {
        separator = UIView()
        separator?.backgroundColor = .lightGray
        separator?.alpha = 0.5
        
        addSubview(separator!)
        separator?.snp.makeConstraints {
            
            if leftLabel?.text == "Email:" {
                $0.left.equalTo(self.snp.left).offset(16)
                $0.right.equalTo(self.snp.right).offset(-16)
            } else {
                $0.left.equalTo((rightLabel?.snp.left)!)
                $0.right.equalTo((rightLabel?.snp.right)!)
            }
            $0.height.equalTo(1)
            let leftHeight = leftLabel?.text?.height(withConstrainedWidth: 30, font: UIFont.systemFont(ofSize: 14))
            let rightHeight = rightLabel?.text?.height(withConstrainedWidth: 30, font: UIFont.systemFont(ofSize: 14))
            
            if leftHeight! > rightHeight! {
                $0.top.equalTo((leftLabel?.snp.bottom)!).offset(8)
            } else {
                $0.top.equalTo((rightLabel?.snp.bottom)!).offset(8)
            }
        }
        self.layoutIfNeeded()
    }
    
    private func setupRightLabel() {
        rightLabel = UILabel()
        rightLabel?.text = ""
        addSubview(rightLabel!)
        rightLabel?.textColor = .black
        rightLabel?.numberOfLines = 0
        rightLabel?.font = UIFont.systemFont(ofSize: 14)
        rightLabel?.lineBreakMode = .byWordWrapping
        proCellLayout.initRightLabel(leftLabel!, rightLabel!)
    }
    
    private func setupLeftLabel() {
        leftLabel = UILabel()
        leftLabel?.text = ""
        leftLabel?.numberOfLines = 0
        leftLabel?.font = UIFont.systemFont(ofSize: 14)
        leftLabel?.lineBreakMode = .byWordWrapping
        addSubview(leftLabel!)
        leftLabel?.textColor = .lightGray
        proCellLayout.initLeftLabel(leftLabel!)
    }
    
}

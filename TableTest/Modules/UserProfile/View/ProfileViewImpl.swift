//
//  ProfileViewImpl.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


class ProfileViewImpl: UIView {
    
    
    var sampleViewModel: CustomViewModel?
    
    let myModel: [CustomViewModel.TableViewCellModel] = [CustomViewModel.TableViewCellModel(naming: "Name:", description: "User Name"),CustomViewModel.TableViewCellModel(naming: "Username:", description: "UserName_Sample"),CustomViewModel.TableViewCellModel(naming: "Email:", description: "User@gmail.com"),CustomViewModel.TableViewCellModel(naming: "Profession:", description: "ProfessionOfUser"),CustomViewModel.TableViewCellModel(naming: "Industry:", description: "UserProfession"),CustomViewModel.TableViewCellModel(naming: "Skills:", description: "Ninja Warrior,BodyWeight Fitness,DietaryAdvice,CrossFit,Boxing,Skill6,Skill7,Skill8,Skill9"),CustomViewModel.TableViewCellModel(naming: "Location Of Services:", description: "UserLocation")]
    
    var tableView: UITableView!
    
    private let  profileTableViewLayout =  ProfileTableViewLayout()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupViews() {
        self.tableView = UITableView()
        tableView.contentInset = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 0.0, right: 0.0)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ProCell.self, forCellReuseIdentifier: "ProCell")
        tableView.register(UserImageCell.self, forCellReuseIdentifier: "UserImageCell")
        tableView.register(ProfessionCell.self, forCellReuseIdentifier: "ProfessionCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UITableViewHeaderFooterView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        self.addSubview(tableView)
        profileTableViewLayout.initial(tableView)
        tableView.separatorStyle = .none
        tableView.reloadData()
    }
}

extension ProfileViewImpl: ProfileView {
    
}

extension ProfileViewImpl: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 92
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else {
            return myModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let firstCell = tableView.dequeueReusableCell(withIdentifier: "UserImageCell", for: indexPath) as? UserImageCell
            return firstCell!
        } else {
            if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessionCell", for: indexPath) as? ProfessionCell
                return cell!
            }
            else  {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProCell", for: indexPath) as? ProCell
                cell?.viewModel = myModel[indexPath.row]
                return cell!
            }
        }
    }
}

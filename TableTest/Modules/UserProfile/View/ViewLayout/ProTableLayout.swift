//
//  ProTableLayout.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit


struct ProCellTableLayout {
    
    func professionalInfoInit(_ view: UILabel) {
        guard let superview =  view.superview else { return }
        view.snp.makeConstraints {
            $0.left.equalTo(superview.snp.left).offset(16)
            $0.top.equalTo(superview.snp.top).offset(16)
        }
    }
    
    func initial(userImage: UIImageView) {
        guard let superview =  userImage.superview else { return }
        userImage.snp.makeConstraints {
            $0.top.equalTo(superview.snp.top).offset(16)
            $0.width.equalTo(60)
            $0.height.equalTo(60)
            $0.centerX.equalTo(superview.snp.centerX)
        }
    }
    
    func initLeftLabel(_ leftView: UILabel ) {
        guard let superview = leftView.superview else { return }
        leftView.snp.makeConstraints {
            $0.left.equalTo(superview.snp.left).offset(16)
            $0.top.equalTo(superview.snp.top).offset(16)
            $0.right.equalTo(superview.snp.centerX).offset(-60)
        }
    }
    
    func initRightLabel(_ leftView: UILabel, _ rightView: UILabel ) {
        guard let superview = rightView.superview else { return }
        rightView.snp.makeConstraints {
            $0.left.equalTo(superview.snp.centerX).offset(-32)
            $0.top.equalTo(superview.snp.top).offset(16)
            $0.right.equalTo(superview.snp.right).offset(-16)
            $0.bottom.equalTo(superview.snp.bottom)
        }
    }
}

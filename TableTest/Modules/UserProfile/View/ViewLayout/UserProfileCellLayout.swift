//
//  UserProfileCellLayout.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

struct UserProfileCellLayout {
    
    func initial(userImage: UIImageView) {
        guard let superview =  userImage.superview else { return }
        userImage.snp.makeConstraints {
            $0.top.equalTo(superview.snp.top).offset(16)
            $0.width.equalTo(60)
            $0.height.equalTo(60)
            $0.centerX.equalTo(superview.snp.centerX)
        }
    }
    func nameInit(userImage: UIImageView, name: UILabel) {
        guard let superview =  name.superview else { return }
        name.snp.makeConstraints {
            $0.top.equalTo(userImage.snp.bottom).offset(16)
            $0.left.equalTo(superview.snp.left).offset(16)
        }
    }
    func currentNameInit(name: UILabel, currentName: UILabel) {
        guard let superview =  name.superview else { return }
        currentName.snp.makeConstraints {
            $0.top.equalTo(name.snp.top)
            $0.left.equalTo(superview.snp.centerX).offset(-32)
            $0.right.equalTo(superview.snp.right).offset(-16)
        }
    }
    func userNameInit(name: UILabel, userName: UILabel) {
        guard let superview =  userName.superview else { return }
        userName.snp.makeConstraints {
            $0.top.equalTo(name.snp.bottom).offset(16)
            $0.left.equalTo(superview.snp.left).offset(16)
        }
    }
    func currentUserNameInit(userName: UILabel, currentUserName: UILabel) {
        guard let superview =  currentUserName.superview else { return }
        currentUserName.snp.makeConstraints {
            $0.top.equalTo(userName.snp.top)
            $0.left.equalTo(superview.snp.centerX).offset(-32)
            $0.right.equalTo(superview.snp.right).offset(-16)
        }
    }
    func userEmailInit(userName: UILabel, userEmail: UILabel) {
        guard let superview =  userEmail.superview else { return }
        userEmail.snp.makeConstraints {
            $0.top.equalTo(userName.snp.bottom).offset(16)
            $0.left.equalTo(superview.snp.left).offset(16)
            //            $0.width.equalTo(60)
        }
    }
    func currentUserEmailInit(userEmail: UILabel, currentEmail: UILabel) {
        guard let superview = currentEmail.superview else { return }
        currentEmail.snp.makeConstraints {
            $0.top.equalTo(userEmail.snp.top).offset(0)
            $0.left.equalTo(superview.snp.centerX).offset(-32)
            $0.right.equalTo(superview.snp.right).offset(-16)
        }
    }
    
    func userProfessionalInfoInit(userProfessionalInfo: UILabel, userEmail: UILabel) {
        guard let superview =  userProfessionalInfo.superview else { return }
        userProfessionalInfo.snp.makeConstraints {
            $0.left.equalTo(superview.snp.left).offset(16)
            $0.right.equalTo(superview.snp.right).offset(-16)
            $0.top.equalTo(userEmail.snp.bottom).offset(16)
        }
    }
    func userProfessionInit(userProfessionalInfo: UILabel, userProfession: UILabel) {
        guard let superview =  userProfession.superview else { return }
        userProfession.snp.makeConstraints {
            $0.left.equalTo(superview.snp.left).offset(16)
            $0.top.equalTo(userProfessionalInfo.snp.bottom).offset(16)
        }
    }
    
    func industryInit(industry: UILabel, userProfession: UILabel) {
        guard let superview =  industry.superview else { return }
        industry.snp.makeConstraints {
            $0.left.equalTo(superview.snp.left).offset(16)
            $0.top.equalTo(userProfession.snp.bottom).offset(16)
        }
    }
    
    func skillsInit(industry: UILabel, skills: UILabel) {
        guard let superview =  skills.superview else { return }
        skills.snp.makeConstraints {
            $0.left.equalTo(superview.snp.left).offset(16)
            $0.top.equalTo(industry.snp.bottom).offset(16)
        }
    }
    
    func currentSkills(skills: UILabel, currentSKills: UILabel) {
      guard let superview = currentSKills.superview else { return }
        currentSKills.snp.makeConstraints {
            $0.left.equalTo(superview.snp.centerX).offset(-32)
            $0.top.equalTo(skills.snp.top).offset(0)
            $0.right.equalTo(superview.snp.right).offset(-16)
//            $0.height.equalTo(160)
        }
    }
}

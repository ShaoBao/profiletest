//
//  UserProfileViewControllerImpl.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation
import UIKit



class UserProfileViewControllerImpl: UIViewController {
    
    var interactor: UserProfileInteractor!
    
     private var profileView: ProfileView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.backgroundColor = UIColor.orange
    }
    
    init() {
        super.init(nibName: .none, bundle: .none)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        let profileView = ProfileViewImpl()
        profileView.frame = view.bounds
        profileView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.profileView = profileView
        view.addSubview(profileView)
        self.title = "My Profile"
    }
}

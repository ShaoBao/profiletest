//
//  UserAssembly.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation



class UserAssembly {
     static func configureModule() -> UserProfileViewControllerImpl {
      let viewController = UserProfileViewControllerImpl()

//      let userService = UserServiceImpl()
      let router = UserProfileRouterImpl(viewController: viewController)
      let presenter = UserProfilePresenterImpl(controller: viewController, router: router)
      let interactor = UserProfileInteractorImpl(presenter: presenter)

        viewController.interactor = interactor
        interactor.viewController = viewController
        presenter.viewController = viewController
        router.viewController = viewController
        
        return viewController
     }
}

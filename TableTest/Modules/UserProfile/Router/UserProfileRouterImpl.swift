//
//  UserProfileRouterImpl.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class UserProfileRouterImpl {
    
    weak var viewController: UserProfileViewControllerImpl?
    
    init(viewController: UserProfileViewControllerImpl) {
        self.viewController = viewController
    }
    
}

extension UserProfileRouterImpl: UserProfileRouter {
    func someFunc() {
        
    } 
    
}

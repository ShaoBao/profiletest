//
//  UserPresenterImpl.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class UserProfilePresenterImpl {
    
    weak var viewController: UserProfileViewControllerImpl?
    private let router: UserProfileRouter
    
    init (controller: UserProfileViewControllerImpl, router: UserProfileRouter) {
        self.viewController = controller
        self.router = router
    }
}

extension UserProfilePresenterImpl: UserProfilePresenter {
    
}

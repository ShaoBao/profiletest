//
//  UserProfileInteractorImpl.swift
//  TableTest
//
//  Created by Gleb on 13/05/2020.
//  Copyright © 2020 Gleb. All rights reserved.
//

import Foundation


class UserProfileInteractorImpl {
    
    
//      private let userService: UserService?
      private let presenter: UserProfilePresenter
      weak var viewController: UserProfileViewControllerImpl?
    
    init(presenter: UserProfilePresenter) {
        self.presenter = presenter
    }
    
}

extension UserProfileInteractorImpl: UserProfileInteractor {
    
}
